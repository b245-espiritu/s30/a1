// Getting the count of total number of fruits on Sale
db.fruits.aggregate(
    [
        {$match: {onSale:true}},
        {$count: 'fruitsOnSale'}
    ]
)


// Gettting the count the total number of fruits with stock more that or equal 20.
db.fruits.aggregate(
    [
        {$match: {stock: {$gte:20}}},
        {$count: 'enoughStock'}
    ]
)


// Getting the average price of fruits on Sale per supplier
db.fruits.aggregate(
    [
        {$match:{onSale:true}},
        {$group:{_id:"$supplier_id", avg_price:{$avg: "$price"} }}
    ]
)


// Getting highest price of a fruit on Sale per supplier
db.fruits.aggregate(
    [   
        {$match:{onSale:true}},
        {$group:{_id:"$supplier_id", max_price:{$max: "$price"} }},
        {$sort: {max_price:1}}
    ]
)

// Getting lowest price of a fruit on Sale per supplier
db.fruits.aggregate(
    [
        {$match:{onSale:true}},
        {$group:{_id:"$supplier_id", min_price:{$min: "$price"} }},
        {$sort: {min_price:1}}
    ]
)
